# IOTA Crypto Core FPGA Module


Technical specifications of the IOTA Crypto Core Module (with ICCFPGA-default-core)
- XC7S50 Spartan 7 Series 
- 16MB-Flash (used for configuration, accessible from the Soft-CPU)
- ATECC608A Secure Element for 8 seeds
- RISC-V compatible soft-cpu with 128kB ROM, 128kB RAM, 4kB Supervisor-RAM running at 100MHz
- Hardware accelerators for Proof-of-Work (Curl-P81), Keccak384, Troika (all single cycle per hashing-round) and type-conversions.
- JTAG for FPGA
- JTAG for RISC-V (full debugging support including SemiHosting)
- 19 GPIOs (alternate functions: 2 SPI-Master, I2C, UART)
- 16 spare GPIOs (not used in ICCFPGA)
- FPGA control signals (INIT, PROGRAM from >=V1.2)  on card connector
